const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const port = process.env.PORT || 4000;
const app = express();

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// connect to mongodb
mongoose.connect('mongodb+srv://admin:admin131@zuittbootcamp.kkyuv.mongodb.net/course-booking?retryWrites=true&w=majority' , 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
)

let db = mongoose.connection;

	db.on("error", console.error.bind(console , "Connection error"))

	db.once('open' , ()=> console.log("Connected to the cloud database"))


app.listen(port, () => console.log(`Server is running at port ${port}`))